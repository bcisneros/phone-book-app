const request = require('supertest')
const session = require('supertest-session')
const sinon = require('sinon')

const app = require('../../app')
const ContactService = require('../../src/contacts/ContactService')
const AuthenticationService = require('../../src/auth/AuthenticationService')

let testSession = null
let authenticatedSession
let anonymousSession = null

let loginStub

const loggedInUser = { id: anyId() }

beforeEach(function (done) {
  loginStub = sinon.stub(AuthenticationService, 'login').callsFake(() => loggedInUser)
  testSession = session(app)
  anonymousSession = request(app)
  testSession
    .post('/login')
    .expect(200)
    .end(function (err) {
      if (err) return done(err)
      authenticatedSession = testSession
      return done()
    })
})

afterEach(() => {
  loginStub.restore()
})

const anyContact = () => {
  return {
    id: 1,
    firstname: 'John',
    lastname: 'Doe',
    phone: '123456',
    user_id: loggedInUser.id,
  }
}

function getRandomInt(min = 0, max = 100) {
  return Math.floor(Math.random() * Math.floor(max) + min)
}

function anyId() {
  return getRandomInt(1, 2000)
}

describe('Contact Routes', () => {
  const url = '/contacts'
  describe('POST /contacts', () => {
    let createContactStub

    const doRequest = (params = {}) =>
      (params.session || authenticatedSession).post(url).send(params.payload || {})

    beforeEach(() => {
      createContactStub = sinon.stub(ContactService, 'create').callsFake(() => {
        return {}
      })
    })

    afterEach(() => {
      createContactStub.restore()
    })

    test('is protected route', () => doRequest({ session: anonymousSession }).expect(401))

    // TODO: Check this with Miguel
    test('returns 201 Created', () => doRequest().expect(201))

    test('returns an application.json Content-Type', () =>
      doRequest().expect('Content-Type', /application\/json/))

    test('returns the new contact in the response', () => {
      const newContact = anyContact()
      createContactStub.callsFake(() => newContact)
      return doRequest().expect(res => {
        expect(res.body.contact).toEqual(newContact)
      })
    })

    test('ContactService.create is called once', () => {
      return doRequest().then(() => {
        sinon.assert.calledOnce(createContactStub)
      })
    })

    test('ContactService.create is called with passed payload and loggedInUser', () => {
      const payload = {
        firstname: 'Susan',
        lastname: 'Morris',
        phone: '555666777889',
      }

      return doRequest({ payload }).then(() => {
        sinon.assert.calledWithExactly(createContactStub, loggedInUser.id, payload)
      })
    })
  })

  describe('GET /contacts', () => {
    let getUserContactsStub
    const doRequest = (params = {}) => (params.session || authenticatedSession).get(url)

    beforeEach(() => {
      getUserContactsStub = sinon.stub(ContactService, 'getAll').callsFake(() => {
        return []
      })
    })

    afterEach(() => {
      getUserContactsStub.restore()
    })

    test('is protected route', () => doRequest({ session: anonymousSession }).expect(401))

    test('returns 200 OK', () => doRequest().expect(200))

    test('returns an application.json Content-Type', () =>
      doRequest().expect('Content-Type', /application\/json/))

    test('returns the list of user contacts', () => {
      const userContacts = [anyContact(), anyContact()]
      getUserContactsStub.callsFake(() => userContacts)
      return doRequest().expect(res => {
        expect(res.body.contacts).toEqual(userContacts)
      })
    })

    test('calls ContactService.getAll passing specific loggedInUser.id', () => {
      return doRequest().then(res => {
        sinon.assert.calledWithExactly(getUserContactsStub, loggedInUser.id)
      })
    })
  })

  describe('GET /contacts/:id', () => {
    let getContactStub

    beforeEach(() => {
      getContactStub = sinon.stub(ContactService, 'getById').callsFake(() => {
        return anyContact()
      })
    })

    afterEach(() => {
      getContactStub.restore()
    })

    const doRequest = (params = {}) =>
      (params.session || authenticatedSession).get(`${url}/${params.contactId || anyId()}`)

    test('is protected route', () => doRequest({ session: anonymousSession }).expect(401))

    test('returns 200 OK', () => doRequest().expect(200))

    test('returns an application.json Content-Type', () =>
      doRequest().expect('Content-Type', /application\/json/))

    test('returns the new contact in the response', () => {
      const contact = anyContact()
      getContactStub.callsFake(() => contact)
      return doRequest().expect(res => {
        expect(res.body.contact).toEqual(contact)
      })
    })

    test('returns 404 if the contact does not exist', () => {
      const contact = null
      getContactStub.callsFake(() => contact)
      return doRequest().expect(404)
    })

    test('ContactService.getById is called once', () => {
      return doRequest().then(() => {
        sinon.assert.calledOnce(getContactStub)
      })
    })

    test('ContactService.getById is called with passed id', () => {
      const contactId = anyId()
      return doRequest({ contactId }).then(() => {
        sinon.assert.calledWithExactly(getContactStub, contactId)
      })
    })
  })

  describe('DELETE /contacts/:id', () => {
    let deleteContactStub
    let getContactStub

    beforeEach(() => {
      deleteContactStub = sinon.stub(ContactService, 'deleteById').callsFake(() => {})

      getContactStub = sinon.stub(ContactService, 'getById').callsFake(() => {
        return anyContact()
      })
    })

    afterEach(() => {
      deleteContactStub.restore()
      getContactStub.restore()
    })

    const doRequest = (params = {}) => {
      const id = params.contactId || anyId()

      return (params.session || authenticatedSession).delete(`${url}/${id}`)
    }

    test('is protected route', () => doRequest({ session: anonymousSession }).expect(401))

    test('returns 204 No Content', () => doRequest().expect(204))

    test('response body has no content', () => {
      return doRequest().expect(res => {
        expect(res.body).toEqual({})
      })
    })

    test('returns 404 if the contact does not exist', () => {
      const contact = null
      getContactStub.callsFake(() => contact)
      return doRequest().expect(404)
    })

    test('returns 403 when contact does not belong to loggedIn user', () => {
      const contact = { id: anyId(), user_id: loggedInUser.id + 1 }
      getContactStub.callsFake(() => contact)
      return doRequest().expect(403)
    })

    test('ContactService.getById is called once', () => {
      return doRequest().then(() => {
        sinon.assert.calledOnce(getContactStub)
      })
    })

    test('ContactService.deleteById is called once', () => {
      return doRequest().then(() => {
        sinon.assert.calledOnce(deleteContactStub)
      })
    })

    test('ContactService.getById is called with passed id', () => {
      const contactId = anyId()

      return doRequest({ contactId }).then(() => {
        sinon.assert.calledWithExactly(getContactStub, contactId)
      })
    })

    test('ContactService.deleteById is called with passed id', () => {
      const contactId = anyId()
      getContactStub.callsFake(() => ({ id: contactId, user_id: loggedInUser.id }))
      return doRequest({ contactId }).then(() => {
        sinon.assert.calledWithExactly(deleteContactStub, contactId)
      })
    })
  })
})
