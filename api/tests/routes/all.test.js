const request = require('supertest')
const session = require('supertest-session')
const listEndpoints = require('express-list-endpoints')
const sinon = require('sinon')

const app = require('../../app')

const AuthenticationService = require('../../src/auth/AuthenticationService')

let testSession = null
let authenticatedSession

let loginStub

beforeEach(function (done) {
  loginStub = sinon.stub(AuthenticationService, 'login').callsFake(() => {
    return { id: 1 }
  })
  testSession = session(app)
  anonymousSession = request(app)
  testSession
    .post('/login')
    .expect(200)
    .end(function (err) {
      if (err) return done(err)
      authenticatedSession = testSession
      return done()
    })
})

afterEach(() => {
  loginStub.restore()
})

describe('Unknown routes', () => {
  const url = '/unknown'
  describe('POST /unknown', () => {
    const doRequest = (params = {}) =>
      (params.session || authenticatedSession).post(url).send(params.payload || {})
    test('returns 404 Not Found', () => doRequest().expect(404))
  })

  describe('GET /unknown', () => {
    const doRequest = (params = {}) => (params.session || authenticatedSession).get(url)
    test('returns 404 Not Found', () => doRequest().expect(404))
  })
})

const endpoints = listEndpoints(app).map(endpoint => {
  endpoint.path = endpoint.path.replace(':id(\\d+)', 1234)
  endpoint.methods = endpoint.methods.map(method => method.toLowerCase())
  return endpoint
})
const methods = ['get', 'post', 'put', 'delete', 'patch']

describe('Not allowed methods', () => {
  const scenarios = endpoints.map(e => {
    return {
      path: e.path,
      methods: methods.filter(m => !e.methods.includes(m)),
    }
  })

  scenarios.forEach(scenario => {
    scenario.methods.forEach(method => {
      test(`${method.toUpperCase()} ${scenario.path} is not allowed`, () => {
        return authenticatedSession[method](scenario.path).expect(405)
      })
    })
  })
})

describe('should return a json object', () => {
  endpoints.forEach(endpoint => {
    endpoint.methods
      .filter(m => m !== 'delete')
      .forEach(method => {
        test(`${method.toUpperCase()} ${endpoint.path}`, () => {
          return authenticatedSession[method](endpoint.path).expect(
            'Content-Type',
            /application\/json/,
          )
        })
      })
  })
})
