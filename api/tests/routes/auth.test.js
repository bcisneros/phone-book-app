const request = require('supertest')
const sinon = require('sinon')

const app = require('../../app')

const AuthenticationService = require('../../src/auth/AuthenticationService')
const InvalidData = require('../../src/common/exceptions/InvalidData')

describe('Authentication Routes', () => {
  let loginStub
  beforeEach(() => {
    loginStub = sinon.stub(AuthenticationService, 'login').callsFake(() => ({}))
  })

  afterEach(() => {
    loginStub.restore()
  })
  describe('POST /login', () => {
    const url = '/login'

    const doRequest = () => request(app).post(url)

    const whenLoggedInUserIs = loggedInUser => loginStub.callsFake(() => loggedInUser)
    const whenErrorIs = error =>
      loginStub.callsFake(() => {
        throw error
      })

    const hasError = message => res => expect(res.body.error).toEqual(message)

    test('returns 200 OK', () => doRequest().expect(200))

    test('calls AuthenticationService.login method once with passed payload', () => {
      const payload = {
        email: 'test@test.com',
        password: 'secret123',
      }
      return doRequest()
        .send(payload)
        .then(() => {
          sinon.assert.calledOnceWithExactly(loginStub, payload)
        })
    })

    test('returns logged in user', () => {
      const loggedInUser = { id: 1, email: 'test@test.com' }

      whenLoggedInUserIs(loggedInUser)

      return doRequest().expect(res => {
        expect(res.body.user).toEqual(loggedInUser)
      })
    })

    test('returns 401 when pass incorrect credentials', () => {
      whenLoggedInUserIs(null)

      return doRequest().expect(401).expect(hasError('Invalid credentials'))
    })

    test('returns 400 when payload is invalid', () => {
      const message = 'Credentials are invalid'

      whenErrorIs(new InvalidData(message))

      return doRequest().expect(400).expect(hasError(message))
    })
  })

  describe('GET /logout', () => {
    const url = '/logout'

    const doRequest = () => request(app).get(url)

    test('returns 200 OK', () => doRequest().expect(200))
  })
})
