const request = require('supertest')
const sinon = require('sinon')

const app = require('../../app')

const UserService = require('../../src/users/UserService')

describe('User routes', () => {
  describe('POST /users', () => {
    let registerUserStub

    const doRequest = () => request(app).post('/users')

    beforeEach(() => {
      registerUserStub = sinon.stub(UserService, 'register').callsFake(() => {
        return {}
      })
    })

    afterEach(() => {
      registerUserStub.restore()
    })

    test('returns 201 Created', () => doRequest().expect(201))

    test('returns application/json Content Type', () =>
      doRequest().expect('Content-Type', /application\/json/))

    test('should call UserService.register with user payload', () => {
      const payload = {
        email: 'test@test.com',
        password: 'secret123',
      }
      return doRequest()
        .send(payload)
        .then(() => {
          sinon.assert.calledOnceWithExactly(registerUserStub, payload)
        })
    })
  })
})
