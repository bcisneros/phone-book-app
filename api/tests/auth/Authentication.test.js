const AuthenticationService = require('../../src/auth/AuthenticationService')
const InvalidData = require('../../src/common/exceptions/InvalidData')
const UserRepository = require('../../src/users/UserRepository')

const sinon = require('sinon')

describe('AuthenticationService', () => {
  describe('login()', () => {
    let getByEmailStub

    beforeEach(() => {
      getByEmailStub = sinon.stub(UserRepository, 'getByEmail').callsFake(() => {
        return { id: 1 }
      })
    })

    afterEach(() => {
      getByEmailStub.restore()
    })

    const throwsError = async (data, expected) => {
      await expect(AuthenticationService.login(data)).rejects.toThrowError(expected)
    }
    test('throws invalid data when email is not present', async () => {
      const data = { email: null }
      await throwsError(data, InvalidData)
    })

    test('email is required', async () => {
      const data = { email: null }
      await throwsError(data, /email is required/)
    })

    test('throws invalid data when password is not present', async () => {
      const data = { password: null }
      await throwsError(data, InvalidData)
    })

    test('throws invalid data when password is not present', async () => {
      const data = { password: null }
      await throwsError(data, /password is required/)
    })

    test('returns null when user is not found by email', async () => {
      const data = { email: 'test@test.com', password: 'secret1234' }
      getByEmailStub.callsFake(() => null)
      const loggedInUser = await AuthenticationService.login(data)

      expect(loggedInUser).toBeNull()
    })

    test('calls UserRepository.getByEmail with passed email', async () => {
      const data = { email: 'test@test.com', password: 'secret1234' }

      getByEmailStub.callsFake(() => {
        return { id: 1, email: 'test@test.com', matchesPassword: () => false }
      })

      await AuthenticationService.login(data)

      sinon.assert.calledOnceWithExactly(getByEmailStub, 'test@test.com')
    })

    test('returns null if the password does not matches with given password', async () => {
      const data = { email: 'test@test.com', password: 'secret1234' }

      getByEmailStub.callsFake(() => {
        return { id: 1, email: 'test@test.com', matchesPassword: () => false }
      })

      const loggedInUser = await AuthenticationService.login(data)

      expect(loggedInUser).toBeNull()
    })

    test('returns the user without password if the password matches with given password', async () => {
      const data = { email: 'test@test.com', password: 'secret1234' }

      getByEmailStub.callsFake(() => {
        return {
          id: 1,
          email: 'test@test.com',
          matchesPassword: () => true,
          password: 'encryptedPasswordHere',
        }
      })

      const loggedInUser = await AuthenticationService.login(data)
      expect(loggedInUser.id).toEqual(1)
      expect(loggedInUser.email).toEqual('test@test.com')
      expect(loggedInUser.password).toBeUndefined()
    })
  })
})
