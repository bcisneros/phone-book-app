const express = require('express')
const ContactController = require('./ContactController')
const ContactService = require('./ContactService')
const router = express.Router()

function parseId(req, res, next) {
  req.params.id = Number.parseInt(req.params.id)
  next()
}

async function checkContactId(req, res, next) {
  const id = req.params.id
  const contact = await ContactService.getById(id)
  req.session.contact = contact
  if (!contact) {
    return res.status(404).json({ error: 'Not Found' })
  }

  if (contact.user_id !== req.session.user.id) {
    return res.status(403).json({ error: 'Forbidden' })
  }
  next()
}

const controller = new ContactController()
router
  .post('/', controller.create)
  .get('/', controller.getUserContacts)
  .get('/:id(\\d+)', parseId, checkContactId, controller.getContactById)
  .delete('/:id(\\d+)', parseId, checkContactId, controller.deleteContactById)

module.exports = router
