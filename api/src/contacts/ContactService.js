const ContactRepository = require('./ContactRepository')
class ContactService {
  static async create(userId, contact) {
    const newContact = await ContactRepository.create({ ...contact, user_id: userId })

    return newContact
  }

  static async getAll(userId) {
    const contacts = await ContactRepository.getAll({
      where: { user_id: userId },
      orderBy: ['firstname', 'lastname'],
    })

    return contacts
  }

  static async getById(id) {
    const contacts = await ContactRepository.getAll({ where: { id } })

    return contacts[0]
  }

  static async deleteById(id) {
    await ContactRepository.delete({ where: { id } })
  }
}

module.exports = ContactService
