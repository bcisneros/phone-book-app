const Contact = require('./Contact')

class ContactRepository {
  static async create(contact, knex) {
    const newContact = await Contact.query(knex).insertAndFetch(contact)
    return newContact
  }

  static async getAll(params = {}, knex) {
    const query = Contact.query(knex)

    if (params.where) {
      query.where(params.where)
    }

    if (params.orderBy) {
      query.orderBy(params.orderBy)
    }

    const contacts = await query

    return contacts
  }

  static async delete(params = {}, knex) {
    const query = Contact.query(knex).delete()

    if (params.where) {
      query.where(params.where)
    }

    await query
  }
}

module.exports = ContactRepository
