const ContactService = require('./ContactService')

class ContactController {
  async create(req, res, next) {
    const user = req.session.user
    const payload = req.body
    const contact = await ContactService.create(user.id, payload)
    res.status(201).json({ contact })
  }

  async getUserContacts(req, res, next) {
    const user = req.session.user
    const contacts = await ContactService.getAll(user.id)
    res.status(200).json({ contacts })
  }

  async getContactById(req, res, next) {
    const contact = req.session.contact
    res.status(200).json({ contact })
  }

  async deleteContactById(req, res, next) {
    const contact = req.session.contact

    await ContactService.deleteById(contact.id)

    res.status(204).end()
  }
}

module.exports = ContactController
