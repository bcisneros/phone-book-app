const express = require('express')
const AuthenticationController = require('./AuthenticationController')
const router = express.Router()

const controller = new AuthenticationController()
router.post('/login', controller.login).get('/logout', controller.logout)

module.exports = router
