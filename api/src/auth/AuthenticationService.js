const InvalidData = require('../common/exceptions/InvalidData')
const UserRepository = require('../users/UserRepository')

const NOT_LOGGED_IN = null

const validate = credentials => {
  const errors = []
  const { email, password } = credentials

  if (!email) {
    errors.push('email is required')
  }

  if (!password) {
    errors.push('password is required')
  }

  if (errors.length) {
    throw new InvalidData(errors)
  }
}
class AuthenticationService {
  static async login(credentials) {
    validate(credentials)

    const user = await UserRepository.getByEmail(credentials.email)

    if (!user || !user.matchesPassword(credentials.password)) {
      return NOT_LOGGED_IN
    }

    delete user.password
    return user
  }
}

module.exports = AuthenticationService
