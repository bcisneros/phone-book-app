const AuthenticationService = require('./AuthenticationService')

class AuthenticationController {
  async login(req, res, next) {
    const payload = req.body
    const user = await AuthenticationService.login(payload)
    if (!user) {
      return res.status(401).json({ error: 'Invalid credentials' })
    }
    req.session.user = user
    res.status(200).send({ user })
  }

  async logout(req, res, next) {
    req.session.destroy()
    res.status(200).json({ success: true })
  }
}

module.exports = AuthenticationController
