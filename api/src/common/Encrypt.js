const bcrypt = require('bcryptjs')

class Encrypt {
  static encrypt(value) {
    const salt = bcrypt.genSaltSync(10)
    var hash = bcrypt.hashSync(value, salt)

    return hash
  }

  static matches(original, hash) {
    return bcrypt.compareSync(original, hash)
  }
}

module.exports = Encrypt
