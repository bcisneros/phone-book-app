const UserRepository = require('./UserRepository')

class UserService {
  static async register(user) {
    const newUser = await UserRepository.create(user)
    return newUser
  }
}

module.exports = UserService
