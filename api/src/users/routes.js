const express = require('express')
const router = express.Router()

const UserController = require('./UserController')

const controller = new UserController()

router.post('/', controller.register)

module.exports = router
