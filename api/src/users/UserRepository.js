const User = require('./User')

class UserRepository {
  static async getByEmail(email, knex) {
    const user = await User.query(knex).where({ email }).limit(1).first()

    return user
  }

  static async create(user, knex) {
    const newUser = await User.query(knex).insertAndFetch(user)
    return newUser
  }
}

module.exports = UserRepository
