const UserService = require('./UserService')

class UserController {
  async register(req, res, next) {
    const payload = req.body
    const user = await UserService.register(payload)
    delete user.password
    res.status(201).json({ user })
  }
}

module.exports = UserController
