const { Model } = require('objection')
const Encrypt = require('../common/Encrypt')

class User extends Model {
  $beforeInsert() {
    this.password = Encrypt.encrypt(this.password)
  }

  static get tableName() {
    return 'users'
  }

  matchesPassword(password) {
    return Encrypt.matches(password, this.password)
  }
}

module.exports = User
