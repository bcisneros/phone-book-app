const { Model } = require('objection')
const Knex = require('knex')
const knexConfig = require('./knexfile')
// Initialize knex.
const knex = Knex(knexConfig)
// Bind all Models to a knex instance. If you only have one database in
// your server this is all you have to do. For multi database systems, see
// the Model.bindKnex() method.
Model.knex(knex)

const express = require('express')
require('express-async-errors')
const session = require('express-session')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const app = express()
const cors = require('cors')
const listEndpoints = require('express-list-endpoints')
const InvalidData = require('./src/common/exceptions/InvalidData')

app.use(logger('dev', { skip: (req, res) => process.env.NODE_ENV === 'test' }))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(cors({ credentials: true, origin: 'http://localhost:3001' }))

// initialize express-session to allow us track the logged-in user across sessions.
app.use(
  session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
      expires: 600000,
    },
  }),
)

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid')
  }
  next()
})

app.use('/users', require('./src/users/routes'))

app.use(require('./src/auth/routes'))

const protectedRoutes = (req, res, next) => {
  if (!req.session.user) {
    return res.status(401).send({ error: 'Not Authorized' })
  }
  next()
}

app.use(protectedRoutes)
app.use('/contacts', require('./src/contacts/routes'))

const endpoints = listEndpoints(app)

// Validate unknow routes either the path or only the used method
app.use(function (req, res, next) {
  const exists = endpoints.some(endpoint => new RegExp(endpoint.path).test(req.path))

  if (exists) {
    return res.status(405).json({ error: 'Not Allowed' })
  }

  res.status(404).json({ error: 'Not Found' })
})

// error handler
const HTTP_SERVER_ERROR = 500
app.use(function (err, req, res, next) {
  if (res.headersSent) {
    return next(err)
  }

  if (err instanceof InvalidData) {
    return res.status(400).json({ error: err.message })
  }

  return res.status(err.status || HTTP_SERVER_ERROR).json({ error: err.message })
})

module.exports = app
