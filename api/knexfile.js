const url = new URL(
  process.env.DATABASE_URL || 'postgres://postgres:postgres@database:5432/phonebook',
)

module.exports = {
  client: 'postgresql',
  searchPath: 'public',
  connection: {
    database: 'phonebook',
    host: 'localhost',
    user: 'postgres',
    // password: url.password,
    // port: url.port,
  },
}
