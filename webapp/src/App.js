import React, { useState } from "react";
import axios from "axios";

import "./styles.css";

import UserForm from "./users/UserForm";
import ContactList from "./contacts/ContactList";
import ContactForm from "./contacts/ContactForm";

const request = axios.create({
  withCredentials: true,
  baseURL: "http://localhost:3000/",
});

export default function App() {
  const [loggedInUser, setLoggedInUser] = useState(null);

  const [contacts, setContacts] = useState([]);

  const register = (user, clearForm) => {
    request.post("users", user).then(
      (response) => {
        alert("User created successfully!");
        login(user, clearForm);
      },
      (error) => {
        alert("An error happened :(");
      }
    );
  };

  const login = (user, clearForm) => {
    request.post("login", user).then(
      (response) => {
        setLoggedInUser(response.data.user);
        clearForm();

        request.get("contacts").then(
          (response) => {
            setContacts(response.data.contacts);
          },
          (error) => {}
        );
      },
      (error) => {
        setLoggedInUser(null);
        alert("An error happened :(");
      }
    );
  };

  const logout = () => {
    request.get("logout").then(
      (response) => {
        setLoggedInUser(null);
      },
      (error) => {
        setLoggedInUser(null);
        alert("An error happened :(");
      }
    );
  };

  const deleteContact = (id) => {
    request.delete(`contacts/${id}`).then(
      (response) => {
        setContacts(contacts.filter((contact) => contact.id !== id));
      },
      (error) => {
        setLoggedInUser(null);
        alert("An error happened :(");
      }
    );
  };

  const createContact = (contact, clearForm) => {
    request.post(`contacts`, contact).then(
      (response) => {
        setContacts([...contacts, response.data.contact]);
        clearForm();
      },
      (error) => {
        alert("An error happened :(");
      }
    );
  };

  return (
    <div className="App">
      <div>
        <h1>Phone Book</h1>
        <section>
          {loggedInUser !== null ? (
            <div>
              <p>
                You are logged in <button onClick={logout}>Logout</button>
              </p>
              <ContactForm onSubmitContact={createContact} />
              <ContactList contacts={contacts} deleteContact={deleteContact} />
            </div>
          ) : (
            <div>
              <UserForm title="Login" onSubmitUser={login} />
              <UserForm title="Register" onSubmitUser={register} />
            </div>
          )}
        </section>
      </div>
    </div>
  );
}
