import React, { useState } from "react";

const UserForm = (props) => {
  const { title = "User Form", onSubmitUser } = props;
  const DEFAULT_USER = { email: "", password: "" };
  const [user, setUser] = useState(DEFAULT_USER);
  const setValue = (e) => {
    const { value, name } = e.target;

    setUser({ ...user, [name]: value });
  };

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        onSubmitUser(user, () => {
          setUser(DEFAULT_USER);
        });
      }}
    >
      <h2>{title}</h2>
      <div>
        <label>Email: </label>
        <input
          className="input"
          name="email"
          type="email"
          required
          value={user.email}
          onChange={setValue}
        />
      </div>
      <div>
        <label>Password: </label>
        <input
          className="input"
          name="password"
          type="password"
          required
          value={user.password}
          onChange={setValue}
        />
      </div>

      <button type="submit">Submit</button>
    </form>
  );
};

export default UserForm;
