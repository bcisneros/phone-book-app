import React from "react";

const ContactList = (props) => {
  const { contacts, deleteContact } = props;
  return (
    <div>
      <h2>Contact List</h2>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Phone</th>
            <th>Address lines</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {contacts.map((contact) => (
            <tr key={contact.id}>
              <td>
                {contact.firstname} {contact.lastname}
              </td>
              <td>{contact.phone}</td>
              <td>{contact.addresses}</td>
              <td>
                <button>View details</button>
                <button
                  onClick={() => deleteContact(contact.id)}
                  className="danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ContactList;
