import React, { useState } from "react";

const ContactForm = (props) => {
  const { onSubmitContact } = props;
  const DEFAULT_CONTACT = { firstname: "", lastname: "", phone: "" };
  const [contact, setContact] = useState(DEFAULT_CONTACT);
  const setValue = (e) => {
    const { value, name } = e.target;

    setContact({ ...contact, [name]: value });
  };

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          onSubmitContact(contact, () => {
            setContact(DEFAULT_CONTACT);
          });
        }}
      >
        <h2>New Contact</h2>
        <div>
          <label>First Name: </label>
          <input
            className="input"
            name="firstname"
            type="text"
            required
            value={contact.firstname}
            onChange={setValue}
          />
        </div>
        <div>
          <label>Last Name: </label>
          <input
            className="input"
            name="lastname"
            type="text"
            required
            value={contact.lastname}
            onChange={setValue}
          />
        </div>

        <div>
          <label>Phone: </label>
          <input
            className="input"
            name="phone"
            type="phone"
            required
            value={contact.phone}
            onChange={setValue}
          />
        </div>

        <button type="submit">Add</button>
      </form>
    </div>
  );
};

export default ContactForm;
